/* Copyright 2000 Pace Micro Technology plc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*----------------------------------------------------------------------*/
/*    Filename    : h.main	                                        */
/*    Author	  : A.Rodger						*/
/*    Date        : Nov 1997						*/
/*    Source      : c++.main						*/
/*    Description : this is essentially a c header file and serves	*/
/*		as a global area for definitions and error functions	*/
/*    Modification history:      		       			*/
/*----------------------------------------------------------------------*/

#ifndef __MAIN
#define __MAIN

	// toolbox includes
#include "wimp.h"
#include "wimplib.h"
#include "toolbox.h"
#include "gadgets.h"
#include "saveas.h"
#include "menu.h"
#include "window.h"
#include "event.h"

	// standard libraries
#include "kernel.h"
#include "swis.h"
#include "limits.h"
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

	// Global Definitions
	//
#define IGNORE(a) (a=a)

#define wimp_version 370

#define our_directory 	"<Print_Tool$Dir>"

#define task_name 	"fish"

#define TRUE	1
#define FALSE	0

#define TEXT_STRING 128
#define millipoint_to_millimetre 2835

	// Message Allocations
#define NUMBER_OF_MESSAGES 10		// number of messages including 0 for quit message

	// Function Definitions
//********************************** Defined in Main.c++ *****

void Debug(char *format, ...);		//debug function that outputs using the DebugMod_Send

int  Error(_kernel_oserror *err);
void BadError(_kernel_oserror *err);

//********************************** Directory Paths *****

#define FILENAME_MESSAGES   "<Print_Tool$Dir>.Messages"  /* Messages file */
#define FILENAME_PAPER_A4	"Printers:PaperA4"
#define FILENAME_PAPER_LETTER	"Printers:PaperLet"
#define FILENAME_PAPER_LEGAL	"Printers:PaperLegal"
#define FILENAME_PAPER	    "Printers:PaperSizes"   /* File containing various paper definitions */
#define PATH_PROLOGUE 	    "Printers:ps.PSfiles."
#define PATH_DEFN 	    "Printers:Printers."    /* Location of printer definition files */
#define PATH_MODULES        "Printers:Modules."
#define PATH_PALETTE        "Printers:Palettes."
#define PATH_SYSTEM_MODULES "Resources:"

#endif
